## Manip docker
 * docker rm $(docker ps -a -q)
 * docker rmi $(docker images -q)
 * docker stats $(docker ps -q)

## Manip dump sql
 * mysqldump -u root -p --databases horsty | gzip > db_horsty.sql.gz