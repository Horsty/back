<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-16
 * Time: 16:11
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package App\Controller
 * @Rest\Route("api/users", name="api_users_")
 * @Rest\Version({"1.0"})
 */

class UserController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
     * @Rest\Get("/me", name="me")
     * @Rest\View(serializerGroups={"me"})
     * @Security("has_role('ROLE_USER')")
     * @return User
     */
    public function me()
    {
        return $this->getUser();
    }

    /**
     * @Rest\Get("/{id}", name="get_id")
     * @Rest\View(serializerGroups={"user"})
     * @param User $user
     * @return User
     */
    public function getById(User $user)
    {
        return $user;
    }

    /**
     * @Rest\Put("/{id}", name="put")
     * @Rest\Patch("/{id}", name="patch")
     * @Rest\View(serializerGroups={"user"})
     * @param Request $request
     * @param User $data
     * @return mixed
     * @Security("has_role('ROLE_USER') and user.getId() == data.getId()")
     */
    public function put(Request $request, User $data)
    {
        $form = $this
            ->createForm(UserType::class, $data, ['method' => $request->getMethod()])
            ->handleRequest($request)
        ;

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($data);
            $em->flush();

            return $data;
        }

        return $form;
    }

    /**
     * @Rest\Post("/", name="post")
     * @Rest\View(
     *     serializerGroups={"user"},
     *     statusCode=Response::HTTP_CREATED)
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function post(Request $request)
    {
        $user = new User();
        $form = $this
            ->createForm(UserType::class, $user, ['method' => $request->getMethod()])
            ->handleRequest($request)
        ;

        if($form->isSubmitted() && $form->isValid()) {
            // le mot de passe en claire est encodé avant la sauvegarde
            $encoded = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);
            $token = base64_encode(random_bytes(50));
            $user->setApiToken($token);

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $user;
        }

        return $form;
    }

}