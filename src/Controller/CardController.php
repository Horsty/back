<?php

/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-10
 * Time: 22:23
 */

namespace App\Controller;

use App\Entity\Card;
use App\Form\CardImportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Category;

/**
 * Class CardController
 * @package App\Controller
 * @Rest\Route("api/cards", name="api_cards_")
 * @Rest\Version({"1.0"})
 */

class CardController extends AbstractController
{
    /**
     * Get all cards
     *
     * @Rest\Get()
     * @Rest\View(serializerGroups={"card"})
     * @param Request $request
     * @return object[]
     */
    public function getCardsAction(Request $request, PaginatorInterface $paginator)
    {
        // return $this->getDoctrine()
        // ->getRepository(Card::class)
        // ->findAll();

        $em = $this->getDoctrine()->getManager();
        $dql = "select c.id from App\Entity\Card c";
        $query = $em->createQuery($dql);
        $query->setMaxResults(10);
        $cards = $query->getResult();
        return $cards;

        // $entityManager = $this->getDoctrine()->getManager();
        // $dql = "SELECT c FROM cards c";
        // $query = $entityManager->createQuery($dql);
        // $cards = $query->getResult();
        
        // $categories = $entityManager
        //     ->getRepository(Category::class)
        //     ->findBy(array('card' => $cards));
        
        // $cardsByCategory = [];
        // foreach ($categories as $category) {
        //     $companyId = $employee->getCompany()->getId();
        //     $cardsByCategory[$companyId][] = $employee;
        // }

/**        $query = $this->getDoctrine()
            ->getRepository(Card::class)
            ->getList()
            ->addSelect('cg', 'lk')
            ->join('c.categories', 'cg')
            ->join('c.links','lk');
dump($query->getQuery());*/
        // $em = $this->getDoctrine()->getManager();
        // // Get some repository of data, in our case we have an Appointments entity
        // $cards = $em->getRepository(Card::class);
                
        // // Find all the data on the Appointments table, filter your query as you need
          //$allCardQuery = $cards->createQueryBuilder('c')
            //  ->getQuery();

        $query = $this->getDoctrine()
            ->getRepository(Card::class)
            ->getList()
        ;

        $cards = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page',1),
            10
        );

        dump($cards);
//         $query = $em->getRepository('Acme\FOOBundle\Entity\BAR')->findAll();
// $paginator = $this->get('knp_paginator');
// requests = $paginator->paginate(
//     $query,
//     $this->get('request')->query->get('page', 1), 
//     5
// );
        return $cards->getItems();
    }

    /**
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"card"})
     * @param Card $card
     * @return Card
     */
    public function getCardAction(Card $card)
    {
        return $card;
    }

    /**
     * Creates an Card resource
     * @Rest\View(
     *  statusCode=Response::HTTP_CREATED
     * )
     * @Rest\Post("/")
     * @param Request $request
     * @return Card
     */
    public function postCardAction(Request $request)
    {
        $card = new Card();
        $card->setName($request->get('name'));
        $card->setDescription($request->get('description'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($card);
        $em->flush();
        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return $card;
    }


    public function getForm(Card $card) {
        return $this->createForm(CardImportType::class, $card);
    }


}