<?php

namespace App\Controller;

use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package App\Controller
 * @Rest\Route("/", name="auth_")
 * @Rest\Version({"1.0"})
 */
class AuthController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Rest\Post("register", name="post")
     * @Rest\View(
     *     serializerGroups={"user"},
     *     statusCode=Response::HTTP_CREATED)
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this
            ->createForm(UserType::class, $user, ['method' => $request->getMethod()])
            ->handleRequest($request)
        ;

        if($form->isSubmitted() && $form->isValid()) {
            // le mot de passe en claire est encodé avant la sauvegarde
            $encoded = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);

            $token = base64_encode(random_bytes(50));
            $user->setApiToken($token);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $user;
        }
        return $form;

    }

}