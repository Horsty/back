<?php

namespace App\Repository;

use App\Entity\SpecialSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SpecialSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecialSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecialSet[]    findAll()
 * @method SpecialSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialSetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SpecialSet::class);
    }

    // /**
    //  * @return SpecialSet[] Returns an array of SpecialSet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpecialSet
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
