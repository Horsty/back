<?php

namespace App\Command;

use App\Controller\CardController;
use App\Repository\CardRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FetchCategory extends Command
{
    protected static $defaultName = 'dokkan:fetch-category';

    private $em;
    private $cardController;
    private $categoryRepo;
    private $cardRepo;

    public function __construct(EntityManagerInterface $entityManager, CardController $cardController, CategoryRepository $categoryRepository, CardRepository $cardRepository)
    {
        // Getting doctrine manager
        $this->em = $entityManager;
        // Getting controller
        $this->cardController = $cardController;
        $this->categoryRepo = $categoryRepository;
        $this->cardRepo = $cardRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addOption(
            'name',
            null,
            InputOption::VALUE_OPTIONAL,
            'Name of files.',
            'categories'
        )

            ->setDescription('Import category from a json to sql')
            ->setHelp('This command allows you to create a lot of category...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        // Showing when the script is launched
        $now = new \DateTime();
        $io->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        $this->fetch($input, $output);

        $now = new \DateTime();
        $io->writeln(PHP_EOL . '<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');


        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }

    protected function fetch(InputInterface $input, OutputInterface $output)
    {
        $data = $this->get($input, $output);
        // Turning off doctrine default logs queries for saving memory
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        //$em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 10;
        $i = 1;
        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();
        $missingCard = [];
        // Processing on each row of data
        foreach ($data as $index => $object) {
            $category = $this->categoryRepo->findOneBy(['import_id' => $index]);
            if ($object) {
                foreach ($object['ids'] as $baseId) {
                    $card = $this->cardRepo->findOneBy(['base_id' => $baseId]);

                    if ($card && $category) {
                        $card->addCategory($category);

                        $this->em->persist($card);
                    } else {
                        $missingCard[] = $baseId;
                    }
                }

                // Each 10 cards persisted we flush everything
                if (($i % $batchSize) === 0) {
                    $this->em->flush();
                    // Detaches all objects from Doctrine for memory save
                    $this->em->clear();
                    $now = new \DateTime();
                    $progress->advance($batchSize);
                    $output->writeln(' of cards imported ... | ' . $now->format('d-m-Y G:i:s'));
                }
            }
            $i++;
        }
        // Flushing and clear data on queue
        $this->em->flush();
        $this->em->clear();
        // Ending the progress bar process
        print_r(array_unique($missingCard));
        $progress->finish();
    }

    protected function get(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getOption('name');
        // Getting the Json from filesystem
        $fileName = 'src/ressources/dokkan/'. $name .'.json';

        // Using service for converting Json to PHP Array
        $data = $this->convert($fileName);

        return $data;
    }

    public function convert($filename)
    {
        if(!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $string = file_get_contents($filename);
        $json = json_decode($string, true);

        return $json;
    }
}
