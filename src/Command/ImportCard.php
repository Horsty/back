<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-17
 * Time: 17:22
 */

namespace App\Command;

use App\Controller\CardController;
use App\Controller\UserController;
use App\Entity\Card;
use App\Entity\Link;
use App\Form\CardImportType;
use App\Repository\CardRepository;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;


class ImportCard extends Command
{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'dokkan:import-card';
    private $em;
    private $cardController;
    private $linkRepo;

    public function __construct(EntityManagerInterface $entityManager, CardController $cardController, LinkRepository $linkRepository)
    {
        // Getting doctrine manager
        $this->em = $entityManager;
        // Getting controller
        $this->cardController = $cardController;
        $this->linkRepo = $linkRepository;
        parent::__construct();
    }

    protected function configure()
    {
        // ...
        $this->addOption(
            'name',
            null,
            InputOption::VALUE_OPTIONAL,
            'Name of files.',
            'cards'
        )

            ->setDescription('Import card from a json to sql')
            ->setHelp('This command allows you to create a lot of cards...')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ...
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing Json on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln(PHP_EOL . '<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from Json
        $data = $this->get($input, $output);

        // Turning off doctrine default logs queries for saving memory
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        //$em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 10;
        $i = 1;
        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();

        // Processing on each row of data
        foreach ($data as $index => $object) {
            if ($object) {
                $card = new Card();
                //$decoded = json_decode($object, true);
                $decoded = $object;
                $form = $this->cardController->getForm($card);
                $form->submit($decoded);
                $card->setName($decoded['character_name']);
                foreach ($decoded['links'] as $link) {
                    $linkEntity = $this->linkRepo
                        ->findOneBy(
                        [
                            'importId' => $link['id']
                        ]
                    )
                    ;

                    $card->addLink($linkEntity);
                }

                $this->em->persist($card);

                // Each 10 cards persisted we flush everything
                if (($i % $batchSize) === 0) {
                    $this->em->flush();
                    // Detaches all objects from Doctrine for memory save
                    $this->em->clear();
                    $now = new \DateTime();
                    $progress->advance($batchSize);
                    $output->writeln(' of cards imported ... | ' . $now->format('d-m-Y G:i:s'));
                }
            } else {
                $progress->advance();
                echo "Error" . $index . PHP_EOL;
            }
            $i++;
        }
        // Flushing and clear data on queue
        $this->em->flush();
        $this->em->clear();
        // Ending the progress bar process
        $progress->finish();
    }

    protected function get(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getOption('name');
        // Getting the Json from filesystem
        $fileName = 'src/ressources/dokkan/'. $name .'.json';

        // Using service for converting Json to PHP Array
        $data = $this->convert($fileName);

        return $data;
    }

    /**
     * @TODO créer un repository / service pour faire cette méthode
     * @param $filename
     * @return bool|mixed
     */
    public function convert($filename)
    {
        if(!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $string = file_get_contents($filename);
        $json = json_decode($string, true);

        return $json;
    }

}