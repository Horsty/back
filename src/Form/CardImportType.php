<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-21
 * Time: 23:27
 */

namespace App\Form;


use App\Entity\Card;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CardImportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name')
            ->add('description')
            ->add('base_id')
            ->add('awoken_id')
            ->add('dokkan_id')
            ->add('dokkan_awakable')
            ->add('potential_board_id')
            ->add('character_name')
            ->add('character_id')
            ->add('unique_info_id')
            ->add('card_name')
            ->add('resource_id')
            ->add('card_type')
            ->add('card_subtype')
            ->add('base_hp_init')
            ->add('base_hp_max')
            ->add('base_atk_init')
            ->add('base_atk_max')
            ->add('base_def_init')
            ->add('base_def_max')
            ->add('base_avg_init')
            ->add('base_avg_max')
            ->add('awoken_hp_init')
            ->add('awoken_hp_max')
            ->add('awoken_atk_init')
            ->add('awoken_atk_max')
            ->add('awoken_def_init')
            ->add('awoken_def_max')
            ->add('awoken_avg_init')
            ->add('awoken_avg_max')
            ->add('pot_hp_max')
            ->add('pot_atk_max')
            ->add('pot_def_max')
            ->add('pot_avg_max')
            ->add('base_open_at')
            ->add('base_rarity')
            ->add('base_element')
            ->add('base_cost')
            ->add('base_lv_max')
            ->add('base_skill_lv_max')
            ->add('base_grow_type')
            ->add('base_exp_type')
            ->add('base_price')
            ->add('base_training_exp')
            ->add('base_eball_min')
            ->add('base_eball_num100')
            ->add('base_eball_max')
            ->add('base_eball_max_num')
            ->add('base_selling_exchange_point')
            ->add('awoken_open_at')
            ->add('awoken_rarity')
            ->add('awoken_element')
            ->add('awoken_cost')
            ->add('awoken_lv_max')
            ->add('awoken_skill_lv_max')
            ->add('awoken_grow_type')
            ->add('awoken_exp_type')
            ->add('awoken_price')
            ->add('awoken_training_exp')
            ->add('awoken_eball_min')
            ->add('awoken_eball_num100')
            ->add('awoken_eball_max')
            ->add('awoken_eball_max_num')
            ->add('awoken_selling_exchange_point')
            ->add('pot_skill_lv_max')
            ->add('leader_id')
            ->add('leader_name')
            ->add('leader_description')
            ->add('passive_skill_set_id')
            ->add('base_awakening_set_id')
            ->add('dokkan_awakening_set_id')
            ->add('rarity')
            ->add('special')
            ->add('special_set')
            ->add('special_detail')
            ->add('passive_name')
            ->add('passive_description')
            ->add('display_value')
            ->add('base_awakening_medals')
            ->add('dokkan_awakening_medals')
            ->add('dokkan_zeni')
            ->add('effect')
            ->add('imgur_thumb')
            ->add('uri')
            ->add('thumb')
            ->add('hash')
            ->add('dokkan_count')
            ->add('eza')
            ->add('extreme_z')
            ->add('reverse_thumb')
            ->add('assets')
            ->add('dokkan_thumb')
            ->add('s3_base')
            ->add('dataset')
            ->add('ticks')
            ->add('links_html')
            ->add('categories')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Card::class
        ));
    }

}