<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinkRepository")
 * @ORM\Table(name="link")
 */
class Link
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Assert\Uuid
     * @ORM\Column(type="string", unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $importId;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $uri;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Card", mappedBy="links")
     * @Serializer\Expose()
     * @Serializer\Groups({"link"})
     * @Serializer\Since("1.0")
     */
    private $cards;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $efficacy_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $target_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $influence_type;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $unique_characters;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "link"})
     * @Serializer\Since("1.0")
     */
    private $card_count;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->addLink($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            $card->removeLink($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTargetType()
    {
        return $this->target_type;
    }

    /**
     * @param mixed $target_type
     */
    public function setTargetType($target_type): void
    {
        $this->target_type = $target_type;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getEfficacyType()
    {
        return $this->efficacy_type;
    }

    /**
     * @param mixed $efficacy_type
     */
    public function setEfficacyType($efficacy_type): void
    {
        $this->efficacy_type = $efficacy_type;
    }

    /**
     * @return mixed
     */
    public function getInfluenceType()
    {
        return $this->influence_type;
    }

    /**
     * @param mixed $influence_type
     */
    public function setInfluenceType($influence_type): void
    {
        $this->influence_type = $influence_type;
    }

    /**
     * @return mixed
     */
    public function getUniqueCharacters()
    {
        return $this->unique_characters;
    }

    /**
     * @param mixed $unique_characters
     */
    public function setUniqueCharacters($unique_characters): void
    {
        $this->unique_characters = $unique_characters;
    }

    /**
     * @return mixed
     */
    public function getCardCount()
    {
        return $this->card_count;
    }

    /**
     * @param mixed $card_count
     */
    public function setCardCount($card_count): void
    {
        $this->card_count = $card_count;
    }

    /**
     * @return mixed
     */
    public function getImportId()
    {
        return $this->importId;
    }

    /**
     * @param mixed $importId
     */
    public function setImportId($importId): void
    {
        $this->importId = $importId;
    }



    
}
