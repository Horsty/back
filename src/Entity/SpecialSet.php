<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpecialSetRepository")
 */
class SpecialSet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $eballNumStart;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $specialDescription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $aimTarget;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $increaseRate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvInit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvMax;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $specialBonusLv1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sb1Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sb1Description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $specialBonusLv2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sb2Name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sb2Description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEballNumStart(): ?int
    {
        return $this->eballNumStart;
    }

    public function setEballNumStart(?int $eballNumStart): self
    {
        $this->eballNumStart = $eballNumStart;

        return $this;
    }

    public function getSpecialName(): ?string
    {
        return $this->specialName;
    }

    public function setSpecialName(?string $specialName): self
    {
        $this->specialName = $specialName;

        return $this;
    }

    public function getSpecialDescription(): ?string
    {
        return $this->specialDescription;
    }

    public function setSpecialDescription(?string $specialDescription): self
    {
        $this->specialDescription = $specialDescription;

        return $this;
    }

    public function getAimTarget(): ?int
    {
        return $this->aimTarget;
    }

    public function setAimTarget(?int $aimTarget): self
    {
        $this->aimTarget = $aimTarget;

        return $this;
    }

    public function getIncreaseRate(): ?int
    {
        return $this->increaseRate;
    }

    public function setIncreaseRate(?int $increaseRate): self
    {
        $this->increaseRate = $increaseRate;

        return $this;
    }

    public function getLvInit(): ?int
    {
        return $this->lvInit;
    }

    public function setLvInit(?int $lvInit): self
    {
        $this->lvInit = $lvInit;

        return $this;
    }

    public function getLvMax(): ?int
    {
        return $this->lvMax;
    }

    public function setLvMax(?int $lvMax): self
    {
        $this->lvMax = $lvMax;

        return $this;
    }

    public function getLvBonus(): ?int
    {
        return $this->lvBonus;
    }

    public function setLvBonus(?int $lvBonus): self
    {
        $this->lvBonus = $lvBonus;

        return $this;
    }

    public function getSpecialBonusLv1(): ?int
    {
        return $this->specialBonusLv1;
    }

    public function setSpecialBonusLv1(?int $specialBonusLv1): self
    {
        $this->specialBonusLv1 = $specialBonusLv1;

        return $this;
    }

    public function getSb1Name(): ?string
    {
        return $this->sb1Name;
    }

    public function setSb1Name(?string $sb1Name): self
    {
        $this->sb1Name = $sb1Name;

        return $this;
    }

    public function getSb1Description(): ?string
    {
        return $this->sb1Description;
    }

    public function setSb1Description(?string $sb1Description): self
    {
        $this->sb1Description = $sb1Description;

        return $this;
    }

    public function getSpecialBonusLv2(): ?int
    {
        return $this->specialBonusLv2;
    }

    public function setSpecialBonusLv2(?int $specialBonusLv2): self
    {
        $this->specialBonusLv2 = $specialBonusLv2;

        return $this;
    }

    public function getSb2Name(): ?string
    {
        return $this->sb2Name;
    }

    public function setSb2Name(?string $sb2Name): self
    {
        $this->sb2Name = $sb2Name;

        return $this;
    }

    public function getSb2Description(): ?string
    {
        return $this->sb2Description;
    }

    public function setSb2Description(?string $sb2Description): self
    {
        $this->sb2Description = $sb2Description;

        return $this;
    }
}
