<?php
// src/Entity/Card.php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 * @ORM\Table(name="card")
 */
class Card {
    /**
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     * @Assert\Uuid
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    public $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    public $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="cards", fetch="EAGER")
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    private $categories;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_awakable;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $potential_board_id;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $character_name;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $character_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $unique_info_id;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $card_name;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $resource_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $card_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $card_subtype;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_hp_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_hp_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_atk_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_atk_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_def_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_def_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_avg_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_avg_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_hp_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_hp_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_atk_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_atk_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_def_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_def_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_avg_init;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_avg_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $pot_hp_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $pot_atk_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $pot_def_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $pot_avg_max;
    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     * @Serializer\Since("1.0")
     */
    private $base_open_at;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_rarity;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_element;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_cost;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_lv_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_skill_lv_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_grow_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_exp_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_price;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_training_exp;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_eball_min;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_eball_num100;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_eball_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_eball_max_num;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_selling_exchange_point;
    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     * @Serializer\Since("1.0")
     */
    private $awoken_open_at;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_rarity;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_element;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_cost;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_lv_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_skill_lv_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_grow_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_exp_type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_price;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_training_exp;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_eball_min;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_eball_num100;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_eball_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_eball_max_num;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $awoken_selling_exchange_point;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $pot_skill_lv_max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $leader_id;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $leader_name;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $leader_description;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $passive_skill_set_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_awakening_set_id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_awakening_set_id;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $rarity;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $special;
    /**
     * @ORM\Column(type="json", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $special_set;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $special_detail;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $passive_name;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $passive_description;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $display_value;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $base_awakening_medals;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_awakening_medals;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_zeni;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $effect;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    private $imgur_thumb;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $uri;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $thumb;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $hash;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_count;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $eza;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $extreme_z;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $reverse_thumb;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $assets;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dokkan_thumb;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $s3_base;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $dataset;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $ticks;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Since("1.0")
     */
    private $links_html;    
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Link", inversedBy="cards", fetch="EAGER")
     * @Serializer\Expose()
     * @Serializer\Groups({"card"})
     * @Serializer\Since("1.0")
     */
    private $links;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBaseId()
    {
        return $this->base_id;
    }

    /**
     * @param mixed $base_id
     */
    public function setBaseId($base_id): void
    {
        $this->base_id = $base_id;
    }

    /**
     * @return mixed
     */
    public function getCharacterName()
    {
        return $this->character_name;
    }

    /**
     * @param mixed $character_name
     */
    public function setCharacterName($character_name): void
    {
        $this->character_name = $character_name;
    }

    /**
     * @return mixed
     */
    public function getImgurThumb()
    {
        return $this->imgur_thumb;
    }

    /**
     * @param mixed $imgur_thumb
     */
    public function setImgurThumb($imgur_thumb): void
    {
        $this->imgur_thumb = $imgur_thumb;
    }

    /**
     * @return mixed
     */
    public function getAwokenId()
    {
        return $this->awoken_id;
    }

    /**
     * @param mixed $awoken_id
     */
    public function setAwokenId($awoken_id): void
    {
        $this->awoken_id = $awoken_id;
    }

    /**
     * @return mixed
     */
    public function getDokkanId()
    {
        return $this->dokkan_id;
    }

    /**
     * @param mixed $dokkan_id
     */
    public function setDokkanId($dokkan_id): void
    {
        $this->dokkan_id = $dokkan_id;
    }

    /**
     * @return mixed
     */
    public function getDokkanAwakable()
    {
        return $this->dokkan_awakable;
    }

    /**
     * @param mixed $dokkan_awakable
     */
    public function setDokkanAwakable($dokkan_awakable): void
    {
        $this->dokkan_awakable = $dokkan_awakable;
    }

    /**
     * @return mixed
     */
    public function getPotentialBoardId()
    {
        return $this->potential_board_id;
    }

    /**
     * @param mixed $potential_board_id
     */
    public function setPotentialBoardId($potential_board_id): void
    {
        $this->potential_board_id = $potential_board_id;
    }

    /**
     * @return mixed
     */
    public function getCharacterId()
    {
        return $this->character_id;
    }

    /**
     * @param mixed $character_id
     */
    public function setCharacterId($character_id): void
    {
        $this->character_id = $character_id;
    }

    /**
     * @return mixed
     */
    public function getUniqueInfoId()
    {
        return $this->unique_info_id;
    }

    /**
     * @param mixed $unique_info_id
     */
    public function setUniqueInfoId($unique_info_id): void
    {
        $this->unique_info_id = $unique_info_id;
    }

    /**
     * @return mixed
     */
    public function getCardName()
    {
        return $this->card_name;
    }

    /**
     * @param mixed $card_name
     */
    public function setCardName($card_name): void
    {
        $this->card_name = $card_name;
    }

    /**
     * @return mixed
     */
    public function getResourceId()
    {
        return $this->resource_id;
    }

    /**
     * @param mixed $resource_id
     */
    public function setResourceId($resource_id): void
    {
        $this->resource_id = $resource_id;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->card_type;
    }

    /**
     * @param mixed $card_type
     */
    public function setCardType($card_type): void
    {
        $this->card_type = $card_type;
    }

    /**
     * @return mixed
     */
    public function getCardSubtype()
    {
        return $this->card_subtype;
    }

    /**
     * @param mixed $card_subtype
     */
    public function setCardSubtype($card_subtype): void
    {
        $this->card_subtype = $card_subtype;
    }

    /**
     * @return mixed
     */
    public function getBaseHpInit()
    {
        return $this->base_hp_init;
    }

    /**
     * @param mixed $base_hp_init
     */
    public function setBaseHpInit($base_hp_init): void
    {
        $this->base_hp_init = $base_hp_init;
    }

    /**
     * @return mixed
     */
    public function getBaseHpMax()
    {
        return $this->base_hp_max;
    }

    /**
     * @param mixed $base_hp_max
     */
    public function setBaseHpMax($base_hp_max): void
    {
        $this->base_hp_max = $base_hp_max;
    }

    /**
     * @return mixed
     */
    public function getBaseAtkInit()
    {
        return $this->base_atk_init;
    }

    /**
     * @param mixed $base_atk_init
     */
    public function setBaseAtkInit($base_atk_init): void
    {
        $this->base_atk_init = $base_atk_init;
    }

    /**
     * @return mixed
     */
    public function getBaseAtkMax()
    {
        return $this->base_atk_max;
    }

    /**
     * @param mixed $base_atk_max
     */
    public function setBaseAtkMax($base_atk_max): void
    {
        $this->base_atk_max = $base_atk_max;
    }

    /**
     * @return mixed
     */
    public function getBaseDefInit()
    {
        return $this->base_def_init;
    }

    /**
     * @param mixed $base_def_init
     */
    public function setBaseDefInit($base_def_init): void
    {
        $this->base_def_init = $base_def_init;
    }

    /**
     * @return mixed
     */
    public function getBaseDefMax()
    {
        return $this->base_def_max;
    }

    /**
     * @param mixed $base_def_max
     */
    public function setBaseDefMax($base_def_max): void
    {
        $this->base_def_max = $base_def_max;
    }

    /**
     * @return mixed
     */
    public function getBaseAvgInit()
    {
        return $this->base_avg_init;
    }

    /**
     * @param mixed $base_avg_init
     */
    public function setBaseAvgInit($base_avg_init): void
    {
        $this->base_avg_init = $base_avg_init;
    }

    /**
     * @return mixed
     */
    public function getBaseAvgMax()
    {
        return $this->base_avg_max;
    }

    /**
     * @param mixed $base_avg_max
     */
    public function setBaseAvgMax($base_avg_max): void
    {
        $this->base_avg_max = $base_avg_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenHpInit()
    {
        return $this->awoken_hp_init;
    }

    /**
     * @param mixed $awoken_hp_init
     */
    public function setAwokenHpInit($awoken_hp_init): void
    {
        $this->awoken_hp_init = $awoken_hp_init;
    }

    /**
     * @return mixed
     */
    public function getAwokenHpMax()
    {
        return $this->awoken_hp_max;
    }

    /**
     * @param mixed $awoken_hp_max
     */
    public function setAwokenHpMax($awoken_hp_max): void
    {
        $this->awoken_hp_max = $awoken_hp_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenAtkInit()
    {
        return $this->awoken_atk_init;
    }

    /**
     * @param mixed $awoken_atk_init
     */
    public function setAwokenAtkInit($awoken_atk_init): void
    {
        $this->awoken_atk_init = $awoken_atk_init;
    }

    /**
     * @return mixed
     */
    public function getAwokenAtkMax()
    {
        return $this->awoken_atk_max;
    }

    /**
     * @param mixed $awoken_atk_max
     */
    public function setAwokenAtkMax($awoken_atk_max): void
    {
        $this->awoken_atk_max = $awoken_atk_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenDefInit()
    {
        return $this->awoken_def_init;
    }

    /**
     * @param mixed $awoken_def_init
     */
    public function setAwokenDefInit($awoken_def_init): void
    {
        $this->awoken_def_init = $awoken_def_init;
    }

    /**
     * @return mixed
     */
    public function getAwokenDefMax()
    {
        return $this->awoken_def_max;
    }

    /**
     * @param mixed $awoken_def_max
     */
    public function setAwokenDefMax($awoken_def_max): void
    {
        $this->awoken_def_max = $awoken_def_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenAvgInit()
    {
        return $this->awoken_avg_init;
    }

    /**
     * @param mixed $awoken_avg_init
     */
    public function setAwokenAvgInit($awoken_avg_init): void
    {
        $this->awoken_avg_init = $awoken_avg_init;
    }

    /**
     * @return mixed
     */
    public function getAwokenAvgMax()
    {
        return $this->awoken_avg_max;
    }

    /**
     * @param mixed $awoken_avg_max
     */
    public function setAwokenAvgMax($awoken_avg_max): void
    {
        $this->awoken_avg_max = $awoken_avg_max;
    }

    /**
     * @return mixed
     */
    public function getPotHpMax()
    {
        return $this->pot_hp_max;
    }

    /**
     * @param mixed $pot_hp_max
     */
    public function setPotHpMax($pot_hp_max): void
    {
        $this->pot_hp_max = $pot_hp_max;
    }

    /**
     * @return mixed
     */
    public function getPotAtkMax()
    {
        return $this->pot_atk_max;
    }

    /**
     * @param mixed $pot_atk_max
     */
    public function setPotAtkMax($pot_atk_max): void
    {
        $this->pot_atk_max = $pot_atk_max;
    }

    /**
     * @return mixed
     */
    public function getPotDefMax()
    {
        return $this->pot_def_max;
    }

    /**
     * @param mixed $pot_def_max
     */
    public function setPotDefMax($pot_def_max): void
    {
        $this->pot_def_max = $pot_def_max;
    }

    /**
     * @return mixed
     */
    public function getPotAvgMax()
    {
        return $this->pot_avg_max;
    }

    /**
     * @param mixed $pot_avg_max
     */
    public function setPotAvgMax($pot_avg_max): void
    {
        $this->pot_avg_max = $pot_avg_max;
    }

    /**
     * @return mixed
     */
    public function getBaseOpenAt()
    {
        return $this->base_open_at;
    }

    /**
     * @param mixed $base_open_at
     */
    public function setBaseOpenAt($base_open_at): void
    {
        $this->base_open_at = $base_open_at;
    }

    /**
     * @return mixed
     */
    public function getBaseRarity()
    {
        return $this->base_rarity;
    }

    /**
     * @param mixed $base_rarity
     */
    public function setBaseRarity($base_rarity): void
    {
        $this->base_rarity = $base_rarity;
    }

    /**
     * @return mixed
     */
    public function getBaseElement()
    {
        return $this->base_element;
    }

    /**
     * @param mixed $base_element
     */
    public function setBaseElement($base_element): void
    {
        $this->base_element = $base_element;
    }

    /**
     * @return mixed
     */
    public function getBaseCost()
    {
        return $this->base_cost;
    }

    /**
     * @param mixed $base_cost
     */
    public function setBaseCost($base_cost): void
    {
        $this->base_cost = $base_cost;
    }

    /**
     * @return mixed
     */
    public function getBaseLvMax()
    {
        return $this->base_lv_max;
    }

    /**
     * @param mixed $base_lv_max
     */
    public function setBaseLvMax($base_lv_max): void
    {
        $this->base_lv_max = $base_lv_max;
    }

    /**
     * @return mixed
     */
    public function getBaseSkillLvMax()
    {
        return $this->base_skill_lv_max;
    }

    /**
     * @param mixed $base_skill_lv_max
     */
    public function setBaseSkillLvMax($base_skill_lv_max): void
    {
        $this->base_skill_lv_max = $base_skill_lv_max;
    }

    /**
     * @return mixed
     */
    public function getBaseGrowType()
    {
        return $this->base_grow_type;
    }

    /**
     * @param mixed $base_grow_type
     */
    public function setBaseGrowType($base_grow_type): void
    {
        $this->base_grow_type = $base_grow_type;
    }

    /**
     * @return mixed
     */
    public function getBaseExpType()
    {
        return $this->base_exp_type;
    }

    /**
     * @param mixed $base_exp_type
     */
    public function setBaseExpType($base_exp_type): void
    {
        $this->base_exp_type = $base_exp_type;
    }

    /**
     * @return mixed
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * @param mixed $base_price
     */
    public function setBasePrice($base_price): void
    {
        $this->base_price = $base_price;
    }

    /**
     * @return mixed
     */
    public function getBaseTrainingExp()
    {
        return $this->base_training_exp;
    }

    /**
     * @param mixed $base_training_exp
     */
    public function setBaseTrainingExp($base_training_exp): void
    {
        $this->base_training_exp = $base_training_exp;
    }

    /**
     * @return mixed
     */
    public function getBaseEballMin()
    {
        return $this->base_eball_min;
    }

    /**
     * @param mixed $base_eball_min
     */
    public function setBaseEballMin($base_eball_min): void
    {
        $this->base_eball_min = $base_eball_min;
    }

    /**
     * @return mixed
     */
    public function getBaseEballNum100()
    {
        return $this->base_eball_num100;
    }

    /**
     * @param mixed $base_eball_num100
     */
    public function setBaseEballNum100($base_eball_num100): void
    {
        $this->base_eball_num100 = $base_eball_num100;
    }

    /**
     * @return mixed
     */
    public function getBaseEballMax()
    {
        return $this->base_eball_max;
    }

    /**
     * @param mixed $base_eball_max
     */
    public function setBaseEballMax($base_eball_max): void
    {
        $this->base_eball_max = $base_eball_max;
    }

    /**
     * @return mixed
     */
    public function getBaseEballMaxNum()
    {
        return $this->base_eball_max_num;
    }

    /**
     * @param mixed $base_eball_max_num
     */
    public function setBaseEballMaxNum($base_eball_max_num): void
    {
        $this->base_eball_max_num = $base_eball_max_num;
    }

    /**
     * @return mixed
     */
    public function getBaseSellingExchangePoint()
    {
        return $this->base_selling_exchange_point;
    }

    /**
     * @param mixed $base_selling_exchange_point
     */
    public function setBaseSellingExchangePoint($base_selling_exchange_point): void
    {
        $this->base_selling_exchange_point = $base_selling_exchange_point;
    }

    /**
     * @return mixed
     */
    public function getAwokenOpenAt()
    {
        return $this->awoken_open_at;
    }

    /**
     * @param mixed $awoken_open_at
     */
    public function setAwokenOpenAt($awoken_open_at): void
    {
        $this->awoken_open_at = $awoken_open_at;
    }

    /**
     * @return mixed
     */
    public function getAwokenRarity()
    {
        return $this->awoken_rarity;
    }

    /**
     * @param mixed $awoken_rarity
     */
    public function setAwokenRarity($awoken_rarity): void
    {
        $this->awoken_rarity = $awoken_rarity;
    }

    /**
     * @return mixed
     */
    public function getAwokenElement()
    {
        return $this->awoken_element;
    }

    /**
     * @param mixed $awoken_element
     */
    public function setAwokenElement($awoken_element): void
    {
        $this->awoken_element = $awoken_element;
    }

    /**
     * @return mixed
     */
    public function getAwokenCost()
    {
        return $this->awoken_cost;
    }

    /**
     * @param mixed $awoken_cost
     */
    public function setAwokenCost($awoken_cost): void
    {
        $this->awoken_cost = $awoken_cost;
    }

    /**
     * @return mixed
     */
    public function getAwokenLvMax()
    {
        return $this->awoken_lv_max;
    }

    /**
     * @param mixed $awoken_lv_max
     */
    public function setAwokenLvMax($awoken_lv_max): void
    {
        $this->awoken_lv_max = $awoken_lv_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenSkillLvMax()
    {
        return $this->awoken_skill_lv_max;
    }

    /**
     * @param mixed $awoken_skill_lv_max
     */
    public function setAwokenSkillLvMax($awoken_skill_lv_max): void
    {
        $this->awoken_skill_lv_max = $awoken_skill_lv_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenGrowType()
    {
        return $this->awoken_grow_type;
    }

    /**
     * @param mixed $awoken_grow_type
     */
    public function setAwokenGrowType($awoken_grow_type): void
    {
        $this->awoken_grow_type = $awoken_grow_type;
    }

    /**
     * @return mixed
     */
    public function getAwokenExpType()
    {
        return $this->awoken_exp_type;
    }

    /**
     * @param mixed $awoken_exp_type
     */
    public function setAwokenExpType($awoken_exp_type): void
    {
        $this->awoken_exp_type = $awoken_exp_type;
    }

    /**
     * @return mixed
     */
    public function getAwokenPrice()
    {
        return $this->awoken_price;
    }

    /**
     * @param mixed $awoken_price
     */
    public function setAwokenPrice($awoken_price): void
    {
        $this->awoken_price = $awoken_price;
    }

    /**
     * @return mixed
     */
    public function getAwokenTrainingExp()
    {
        return $this->awoken_training_exp;
    }

    /**
     * @param mixed $awoken_training_exp
     */
    public function setAwokenTrainingExp($awoken_training_exp): void
    {
        $this->awoken_training_exp = $awoken_training_exp;
    }

    /**
     * @return mixed
     */
    public function getAwokenEballMin()
    {
        return $this->awoken_eball_min;
    }

    /**
     * @param mixed $awoken_eball_min
     */
    public function setAwokenEballMin($awoken_eball_min): void
    {
        $this->awoken_eball_min = $awoken_eball_min;
    }

    /**
     * @return mixed
     */
    public function getAwokenEballNum100()
    {
        return $this->awoken_eball_num100;
    }

    /**
     * @param mixed $awoken_eball_num100
     */
    public function setAwokenEballNum100($awoken_eball_num100): void
    {
        $this->awoken_eball_num100 = $awoken_eball_num100;
    }

    /**
     * @return mixed
     */
    public function getAwokenEballMax()
    {
        return $this->awoken_eball_max;
    }

    /**
     * @param mixed $awoken_eball_max
     */
    public function setAwokenEballMax($awoken_eball_max): void
    {
        $this->awoken_eball_max = $awoken_eball_max;
    }

    /**
     * @return mixed
     */
    public function getAwokenEballMaxNum()
    {
        return $this->awoken_eball_max_num;
    }

    /**
     * @param mixed $awoken_eball_max_num
     */
    public function setAwokenEballMaxNum($awoken_eball_max_num): void
    {
        $this->awoken_eball_max_num = $awoken_eball_max_num;
    }

    /**
     * @return mixed
     */
    public function getAwokenSellingExchangePoint()
    {
        return $this->awoken_selling_exchange_point;
    }

    /**
     * @param mixed $awoken_selling_exchange_point
     */
    public function setAwokenSellingExchangePoint($awoken_selling_exchange_point): void
    {
        $this->awoken_selling_exchange_point = $awoken_selling_exchange_point;
    }

    /**
     * @return mixed
     */
    public function getPotSkillLvMax()
    {
        return $this->pot_skill_lv_max;
    }

    /**
     * @param mixed $pot_skill_lv_max
     */
    public function setPotSkillLvMax($pot_skill_lv_max): void
    {
        $this->pot_skill_lv_max = $pot_skill_lv_max;
    }

    /**
     * @return mixed
     */
    public function getLeaderId()
    {
        return $this->leader_id;
    }

    /**
     * @param mixed $leader_id
     */
    public function setLeaderId($leader_id): void
    {
        $this->leader_id = $leader_id;
    }

    /**
     * @return mixed
     */
    public function getLeaderName()
    {
        return $this->leader_name;
    }

    /**
     * @param mixed $leader_name
     */
    public function setLeaderName($leader_name): void
    {
        $this->leader_name = $leader_name;
    }

    /**
     * @return mixed
     */
    public function getLeaderDescription()
    {
        return $this->leader_description;
    }

    /**
     * @param mixed $leader_description
     */
    public function setLeaderDescription($leader_description): void
    {
        $this->leader_description = $leader_description;
    }

    /**
     * @return mixed
     */
    public function getPassiveSkillSetId()
    {
        return $this->passive_skill_set_id;
    }

    /**
     * @param mixed $passive_skill_set_id
     */
    public function setPassiveSkillSetId($passive_skill_set_id): void
    {
        $this->passive_skill_set_id = $passive_skill_set_id;
    }

    /**
     * @return mixed
     */
    public function getBaseAwakeningSetId()
    {
        return $this->base_awakening_set_id;
    }

    /**
     * @param mixed $base_awakening_set_id
     */
    public function setBaseAwakeningSetId($base_awakening_set_id): void
    {
        $this->base_awakening_set_id = $base_awakening_set_id;
    }

    /**
     * @return mixed
     */
    public function getDokkanAwakeningSetId()
    {
        return $this->dokkan_awakening_set_id;
    }

    /**
     * @param mixed $dokkan_awakening_set_id
     */
    public function setDokkanAwakeningSetId($dokkan_awakening_set_id): void
    {
        $this->dokkan_awakening_set_id = $dokkan_awakening_set_id;
    }

    /**
     * @return mixed
     */
    public function getRarity()
    {
        return $this->rarity;
    }

    /**
     * @param mixed $rarity
     */
    public function setRarity($rarity): void
    {
        $this->rarity = $rarity;
    }

    /**
     * @return mixed
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * @param mixed $special
     */
    public function setSpecial($special): void
    {
        $this->special = $special;
    }

    /**
     * @return mixed
     */
    public function getSpecialSet()
    {
        return $this->special_set;
    }

    /**
     * @param mixed $special_set
     */
    public function setSpecialSet($special_set): void
    {
        $this->special_set = $special_set;
    }

    /**
     * @return mixed
     */
    public function getSpecialDetail()
    {
        return $this->special_detail;
    }

    /**
     * @param mixed $special_detail
     */
    public function setSpecialDetail($special_detail): void
    {
        $this->special_detail = $special_detail;
    }

    /**
     * @return mixed
     */
    public function getPassiveName()
    {
        return $this->passive_name;
    }

    /**
     * @param mixed $passive_name
     */
    public function setPassiveName($passive_name): void
    {
        $this->passive_name = $passive_name;
    }

    /**
     * @return mixed
     */
    public function getPassiveDescription()
    {
        return $this->passive_description;
    }

    /**
     * @param mixed $passive_description
     */
    public function setPassiveDescription($passive_description): void
    {
        $this->passive_description = $passive_description;
    }

    /**
     * @return mixed
     */
    public function getDisplayValue()
    {
        return $this->display_value;
    }

    /**
     * @param mixed $display_value
     */
    public function setDisplayValue($display_value): void
    {
        $this->display_value = $display_value;
    }

    /**
     * @return mixed
     */
    public function getBaseAwakeningMedals()
    {
        return $this->base_awakening_medals;
    }

    /**
     * @param mixed $base_awakening_medals
     */
    public function setBaseAwakeningMedals($base_awakening_medals): void
    {
        $this->base_awakening_medals = $base_awakening_medals;
    }

    /**
     * @return mixed
     */
    public function getDokkanAwakeningMedals()
    {
        return $this->dokkan_awakening_medals;
    }

    /**
     * @param mixed $dokkan_awakening_medals
     */
    public function setDokkanAwakeningMedals($dokkan_awakening_medals): void
    {
        $this->dokkan_awakening_medals = $dokkan_awakening_medals;
    }

    /**
     * @return mixed
     */
    public function getDokkanZeni()
    {
        return $this->dokkan_zeni;
    }

    /**
     * @param mixed $dokkan_zeni
     */
    public function setDokkanZeni($dokkan_zeni): void
    {
        $this->dokkan_zeni = $dokkan_zeni;
    }

    /**
     * @return mixed
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * @param mixed $effect
     */
    public function setEffect($effect): void
    {
        $this->effect = $effect;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * @param mixed $thumb
     */
    public function setThumb($thumb): void
    {
        $this->thumb = $thumb;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getDokkanCount()
    {
        return $this->dokkan_count;
    }

    /**
     * @param mixed $dokkan_count
     */
    public function setDokkanCount($dokkan_count): void
    {
        $this->dokkan_count = $dokkan_count;
    }

    /**
     * @return mixed
     */
    public function getEza()
    {
        return $this->eza;
    }

    /**
     * @param mixed $eza
     */
    public function setEza($eza): void
    {
        $this->eza = $eza;
    }

    /**
     * @return mixed
     */
    public function getExtremeZ()
    {
        return $this->extreme_z;
    }

    /**
     * @param mixed $extreme_z
     */
    public function setExtremeZ($extreme_z): void
    {
        $this->extreme_z = $extreme_z;
    }

    /**
     * @return mixed
     */
    public function getReverseThumb()
    {
        return $this->reverse_thumb;
    }

    /**
     * @param mixed $reverse_thumb
     */
    public function setReverseThumb($reverse_thumb): void
    {
        $this->reverse_thumb = $reverse_thumb;
    }

    /**
     * @return mixed
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @param mixed $assets
     */
    public function setAssets($assets): void
    {
        $this->assets = $assets;
    }

    /**
     * @return mixed
     */
    public function getDokkanThumb()
    {
        return $this->dokkan_thumb;
    }

    /**
     * @param mixed $dokkan_thumb
     */
    public function setDokkanThumb($dokkan_thumb): void
    {
        $this->dokkan_thumb = $dokkan_thumb;
    }

    /**
     * @return mixed
     */
    public function getS3Base()
    {
        return $this->s3_base;
    }

    /**
     * @param mixed $s3_base
     */
    public function setS3Base($s3_base): void
    {
        $this->s3_base = $s3_base;
    }

    /**
     * @return mixed
     */
    public function getDataset()
    {
        return $this->dataset;
    }

    /**
     * @param mixed $dataset
     */
    public function setDataset($dataset): void
    {
        $this->dataset = $dataset;
    }

    /**
     * @return mixed
     */
    public function getTicks()
    {
        return $this->ticks;
    }

    /**
     * @param mixed $ticks
     */
    public function setTicks($ticks): void
    {
        $this->ticks = $ticks;
    }

    /**
     * @return mixed
     */
    public function getLinksHtml()
    {
        return $this->links_html;
    }

    /**
     * @param mixed $links_html
     */
    public function setLinksHtml($links_html): void
    {
        $this->links_html = $links_html;
    }






}



