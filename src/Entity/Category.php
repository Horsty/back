<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @ORM\Column(type="string", unique=true)
     * @Assert\Uuid
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "category"})
     * @Serializer\Since("1.0")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "category"})
     * @Serializer\Since("1.0")
     */
    private $name;

    /**
     * @ORM\Column(type="integer", unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"card", "category"})
     * @Serializer\Since("1.0")
     */
    private $import_id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Card", mappedBy="categories")
     * @Serializer\Expose()
     * @Serializer\Groups({"category"})
     * @Serializer\Since("1.0")
     */
    private $cards;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->addCategory($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            $card->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * @param mixed $import_id
     */
    public function setImportId($import_id): void
    {
        $this->import_id = $import_id;
    }


}
