# ./docker/php/Dockerfile
FROM php:7.2-fpm

RUN apt-get update && apt-get install -y \
    git \
    zip \
    unzip

RUN mkdir -p /var/www/api
WORKDIR /var/www/api

RUN docker-php-ext-install pdo_mysql

RUN pecl install apcu-5.1.8
RUN docker-php-ext-enable apcu

RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

WORKDIR /var/www/api

COPY composer.json /var/www/api/composer.json
COPY composer.lock /var/www/api/composer-lock.json

COPY . /var/www/api

RUN PATH=$PATH:/usr/src/app/vendor/bin:bin

RUN composer install

EXPOSE 9000
